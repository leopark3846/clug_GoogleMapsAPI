package com.example.leo.studyapi;

import com.androidtutorialpoint.googlemapsretrofit.POJO.Example;

import retrofit2.Response;

/**
 * Created by leo on 2017-07-17.
 */

public interface AsyncResponse {
    void processFinish(Response<Example> res);
}
