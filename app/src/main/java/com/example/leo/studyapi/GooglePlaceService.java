package com.example.leo.studyapi;

import com.androidtutorialpoint.googlemapsretrofit.POJO.Example;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by leo on 2017-07-16.
 */

public interface GooglePlaceService {
    @GET("maps/api/place/nearbysearch/json")
    Call<Example> getNearbyPlaces(
            @QueryMap Map<String, String> options
    );
    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
