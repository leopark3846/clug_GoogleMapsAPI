package com.example.leo.studyapi;

import android.os.AsyncTask;
import android.util.Log;

import com.androidtutorialpoint.googlemapsretrofit.POJO.Example;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by leo on 2017-07-17.
 */

class NetworkCall extends AsyncTask<Call, Void, Response<Example>> {
    public AsyncResponse delegate = null;
    @Override
    protected Response<Example> doInBackground(Call... params){
        try {
            Call<Example> call = params[0];
            Response<Example> response = call.execute();
            /* response 어떻게 좋게 구현해서 넘겨주는 지 알아봐야됨*/
            return response;
        } catch (Exception e){
            e.notify();
        }
        return null;
    }
    @Override
    /* json 데이터 어떻게 넘겨줄지 생각해봐야됨 */
    protected void onPostExecute(Response<Example> response){
        delegate.processFinish(response);
    }
}
