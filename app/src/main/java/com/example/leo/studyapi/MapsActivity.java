package com.example.leo.studyapi;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;

import com.androidtutorialpoint.googlemapsretrofit.POJO.Example;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, AsyncResponse{

    private GoogleMap mMap;
    JsonArray resultJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        findViewById(R.id.btn_ok).setOnClickListener(mClickListener);
    }
    Button.OnClickListener mClickListener = new View.OnClickListener(){
        public void onClick(View v){
            switch(v.getId()) {
                case R.id.btn_ok:
                    Map<String, String> d = new HashMap<String, String>();
                    d.put("location", "37.5589679,126.937096");
                    d.put("radius", "500");
                    d.put("type", "cafe");
                    d.put("keyword", "카페");
                    d.put("key", "AIzaSyBx7DqG04qR-hXB6wZpyW7pJhDB3OTelC4");

                    GooglePlaceService googlePlaceService = GooglePlaceService.retrofit.create(GooglePlaceService.class);
                    final Call<Example> call = googlePlaceService.getNearbyPlaces(d);
                    NetworkCall n = new NetworkCall();
                    n.delegate = MapsActivity.this;
                    n.execute(call);
            }
        }
    };

    @Override
    public void processFinish(Response<Example> response){
        //Here you will receive the result fired from async class
        //of onPostExecute(result) method.
        try{
            for (int i=0; i < response.body().getResults().size(); i++){
                Double lat = response.body().getResults().get(i).getGeometry().getLocation().getLat();
                Double lng = response.body().getResults().get(i).getGeometry().getLocation().getLng();
                String placeName = response.body().getResults().get(i).getName();
                String vicinity = response.body().getResults().get(i).getVicinity();
                MarkerOptions markerOptions = new MarkerOptions();
                LatLng latLng = new LatLng(lat, lng);
                // Position of Marker on Map
                markerOptions.position(latLng);
                // Adding Title to the Marker
                markerOptions.title(placeName + " : " + vicinity);
                // Adding Marker to the Camera.
                Marker m = mMap.addMarker(markerOptions);
                // Adding colour to the marker
            }
        } catch (Exception e){
            Log.e("My App", "Could not parse malformed JSON: \"" + response.body().toString() + "\"");
        }

    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(37.504, 126.956);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in CAU"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
